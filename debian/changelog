sagemath-database-elliptic-curves (0.8.1-6) unstable; urgency=medium

  * Fix d/watch.
  * Bump standards-version to 4.6.2.

 -- Julien Puydt <jpuydt@debian.org>  Tue, 20 Jun 2023 12:38:09 +0200

sagemath-database-elliptic-curves (0.8.1-5) unstable; urgency=medium

  * Update d/copyright.
  * Declare d/rules doesn't require root.
  * Bump std-vers to 4.5.0.
  * Bump dh compat to 13.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 12 Oct 2020 23:19:11 +0200

sagemath-database-elliptic-curves (0.8.1-4) unstable; urgency=medium

  * New source-only upload to migrate to testing.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 08 Aug 2019 15:28:30 +0200

sagemath-database-elliptic-curves (0.8.1-3) unstable; urgency=medium

  * Move to Python3.
  * Bump dh compat to 12, and drop d/compat.

 -- Julien Puydt <jpuydt@debian.org>  Tue, 23 Jul 2019 07:35:54 +0200

sagemath-database-elliptic-curves (0.8.1-2) unstable; urgency=medium

  * Adapt d/watch to upstream's new layout (no more direct download,
   have to go through a mirror, so I'm leaving only the detection of
   a new version).
  * Made spkg-install copy (not move) files (Closes: #928128).
  * Bump std-vers to 4.4.0.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 15 Jul 2019 09:00:00 +0200

sagemath-database-elliptic-curves (0.8.1-1) experimental; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

  [ Julien Puydt ]
  * New upstream release.
  * Bump std-vers to 4.3.0.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 22 Apr 2019 12:27:53 +0200

sagemath-database-elliptic-curves (0.8-2) unstable; urgency=medium

  * Refresh packaging:
    - Use salsa in Vcs-* fields.
    - Use my debian.org email address.
    - Bump dh compat to 11.
    - Bump std-ver to 4.1.4.

 -- Julien Puydt <jpuydt@debian.org>  Fri, 25 May 2018 11:00:23 +0200

sagemath-database-elliptic-curves (0.8-1) unstable; urgency=medium

  * New upstream release.

 -- Julien Puydt <julien.puydt@laposte.net>  Sat, 22 Aug 2015 13:15:59 +0200

sagemath-database-elliptic-curves (0.7+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #703297)

 -- Julien Puydt <julien.puydt@laposte.net>  Mon, 18 Mar 2013 22:24:17 +0100
